@extends('layouts.app')

@section('content')
<div class="container">
@foreach ($posts as $post)
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                
                <div class="card-header"><img src="{{ $user->avatar }}" width="7%" style="border-radius:50%; margin-right:10px;">{{ $user->name }}</div>
                
                <div class="card-body" style="padding:0;">
                <a href="{{route('post.show',[$post->id])}}"><img src="{{ $post->image }}" width="100%" name="image" ></a>
                </div>
                <div class="card-footer">
                <form class="btn" method=GET action="{{url('/post/'.$post->id.'/like')}}">
                <button class="btn"><i class="far fa-heart" style='font-size:17px'></i></button>    
                </form>    
                <button class="btn"><i class='far fa-comment' style='font-size:17px'></i></button>
                 <br>
                <label style="margin-top:5px; margin-bottom:5">{{$post->likes}} likes</label>
                <br>
                <label ><b>{{ $user->email }}</b></label>
                <label>{{ $post->caption }}</label>
                <br>
                <form class="btn" method=POST action="{{route('post.comment.store', [$post->id])}}">
                @csrf
                <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Komen" name="comment" style="padding:12px 16px">
                <div class="input-group-append">
                <span class="input-group-text"><button class="btn" type="submit">Post</button></span>
                </div>
                </div>
                </form>
                </div> 
            </div>
        </div>
    </div>
    <br>
    @endforeach
</div>
@endsection