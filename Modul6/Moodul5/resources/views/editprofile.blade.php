@extends('layouts.app')

@section('content')
<div class="container">
<div class="row justify-content-center">
    <div class="col-md-6">
    <h1>Edit Profile</h1>
    <br>
    <form method="post" action="{{route('users.update',[$user->id])}}" enctype="multipart/form-data">
    @csrf
        <p style="margin-bottom:5px; margin-left:5px; ">Title</p>
        <input type="text" class="form-control" name="title" placeholder="Insert a Title"><br>
        <p style="margin-bottom:5px; margin-left:5px; margin-top:10px;">Description</p>
        <input type="text" class="form-control" name="description" placeholder="Insert a Description"><br>
        <p style="margin-bottom:5px; margin-left:5px; margin-top:10px;">URL</p>
        <input type="text" class="form-control" name="url" placeholder="Insert a Description"><br>
        <p style="margin-bottom:5px; margin-left:5px; margin-top:10px;">Profile Image</p>
        <input type="file" name="avatar" placeholder="image" style="margin-bottom:35px;"><br>
        <input type="hidden" value="PUT" name="_method">
        <input type="submit" value="Save Profile" style="background-color:#29B4F0; border-color:#DEDEDE ; color:white; border-radius:5px;   ">
    </form>
</div>
</div>
</div>
@endsection
