<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\post;
use App\komentar_posts;
use App\User;
use Auth;   

class KomentarPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $pid)
    {
        $user = Auth::user();
        $coments = new komentar_posts();
        $coments->user_id = $user->id;
        $coments->post_id = $pid;
        $coments->comment = $request->get('comment');

        $coments->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function addComment(Request $request)
    {
        $coments = new komentar_posts();
        $coments->user_id = Auth::user()->id;
        $coments->post_id = $request->post_id;
        $coments->comment = $request->comment;

        $coments->save();
        return redirect()->route('users.index');
    }   
}
