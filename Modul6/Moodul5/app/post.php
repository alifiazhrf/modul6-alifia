<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class post extends Model
{
    protected $fillable = [
        'user_id', 'caption', 'image', 'likes',
    ];

    public function user(){
        return $this->belongsTo(\app\User::class, 'user_id');
    }
    
    public function komentar_posts(){
        return $this->hasMany(\app\komentar_posts::class, 'post_id');
    }
}
