<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            'user_id' => '1',
            'caption' => 'kangen rumah',
            'image' => 'tumblr.png',
        ]);
        DB::table('posts')->insert([
            'user_id' => '1',
            'caption' => 'belajar dulu gaes',
            'image' => 'study.jpeg',
        ]);
        DB::table('posts')->insert([
            'user_id' => '1',
            'caption' => 'belajar sambil ngopi',
            'image' => 'ngopi.jpg',
        ]);
    }
}
