@extends('layouts.app')
@section('content')
<div class="container">
<div class="row justify-content-center">
    <div class="col-md-6">
    <h1>Add New Post</h1>
    <br>
    <form method="post" action="{{route('users.store')}}" enctype="multipart/form-data">
    @csrf
        <p style="margin-bottom:5px; margin-left:5px">Post caption</p>
        <input type="text" class="form-control" name="caption" placeholder="Insert a Caption" ><br>
        <p style="margin-bottom:5px; margin-left:5px">Post image</p>
        <input type="file" name="image" placeholder="image" style="margin-bottom:35px;"><br>
        <input type="submit" value="Add new post" style="background-color:#29B4F0; border-color:#DEDEDE ; color:white; border-radius:5px; padding:6px 16px;">
    </form>
</div>
</div>
</div>
@endsection
