@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center my-5">
        <div class="col-md-4 text-center">
            <img src="{{ asset($user->avatar) }}" style="heigth:300px; width:300px;">
        </div>
        <div class="col-md-6">
            <h4>{{$user->name}}</h4><br>
            <p><a href="{{route('users.edit',[$user->id])}}" >Edit Profile</a><br>
                <b>{{sizeof($posts)}}</b> Post</p>
            <p><b>{{$user->title}}</b><br>{{$user->description}}<br><a href="{{$user->url}}">{{$user->url}}</a> 
            </p>
        </div>
        <div class="col-md-2 text-right">
            <a href="{{route('post.create')}}">Add New Post</a>
        </div>
    </div>
    <div class="row justify-content-left my-1">
    @foreach($posts as $post)
        <div class="col-md-4 my-3">
            <a href="{{route('post.show',[$post->id])}}"><img src="{{asset($post->image)}}" style="width:100%"></a>
        </div>
        @endforeach
    </div>
</div>
@endsection
