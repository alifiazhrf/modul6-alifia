<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class komentar_posts extends Model
{
    protected $fillable = [
        'user_id', 'post_id', 'comment',
    ];
    public function user(){
        return $this->belongsTo(\app\User::class, 'user_id');
    }
    public function post(){
        return $this->belongsTo(\app\post::class, 'post_id');
    }
}
