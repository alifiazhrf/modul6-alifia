<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware('auth')->group(function(){
        Route::resource('users','UserController');
        Route::get('post/{id}/like','PostController@addLike')->name('post.addlike');
        Route::resource('post', 'PostController');
        Route::group(['as'=>'post.','prefix'=>'post/{pid}/'], function(){
            Route::resource('comment', 'KomentarPostController');
        });
        // Route::get('post/{id}/comment','KomentarPostController@addComment')->name('comment.addComment');
});


Route::get('/home', 'HomeController@index')->name('home');
