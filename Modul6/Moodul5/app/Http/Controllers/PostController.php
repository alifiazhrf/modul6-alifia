<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\post;
use App\komentar_posts;
use App\User;
use Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('newpost', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new post;
        $post->user_id=Auth::user()->id;
        $post->caption=$request->get('caption');
        $post->image=$request->file('image')->getClientOriginalName();
        $request->file('image')->move('images',$request->file('image')->getClientOriginalName());
        $post->save();
        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $post = post::find($id);
        $comments = komentar_posts::where('post_id', $post->id)->get();
        // dd($posts);
        return view('detail', compact('user', 'post', 'comments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function addLike($id){
        $post = post::find($id);
        if($post){
            $post->likes = $post->likes+1;
            $post->save();
            $response['success'] = true;
            $response['message'] = "Post berhasil diupdate.";
        } else {
            $response['success'] = false;
            $response['message'] = "Post tidak ditemukan.";
        }

        return back();

        return response($response, 200);
    }
}
