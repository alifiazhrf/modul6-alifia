@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <img src="{{asset($post->image)}}" width="100%">
        </div>
        <div class="col-md-5">
            <img src="{{ asset($user->avatar) }}" width="50" style="border-radius: 50%; border: solid 1px;">
            <b>{{$user->name}}</b> <br>
            <hr>
            <p><b>{{$user->email}}</b> {{$post->caption}}</p>
            <p>
                @foreach($comments as $komentar)
                <b>{{$komentar->user()->first()->email}}</b> {{$komentar->comment}} <br>
                @endforeach
            </p>
            <hr>
            <form class="btn" method=GET action="{{url('/post/'.$post->id.'/like')}}">
                <button class="btn"><i class="far fa-heart" style='font-size:17px'></i></button>    
                </form>    
                <button class="btn"><i class='far fa-comment' style='font-size:17px'></i></button>
                 <br>
                <label style="margin-top:5px; margin-bottom:5">{{$post->likes}} likes</label>
                </form>
                <br>
            
            <form class="btn" method=POST action="{{route('post.comment.store', [$post->id])}}">
                @csrf
                <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Komen" name="comment">
                <div class="input-group-append">
                <span class="input-group-text"><button class="btn" type="submit">Post</button></span>
                </div>
                </div>
                </form>
        </div>
    </div>
</div>
@endsection